# Kernel Webhooks

This repository contains the code for kernel webhooks for various Red Hat
gitlab projects.

These webhooks interact with the kernel projects and apply labels, run CI
jobs, perform BZ checks, among other things.

## Hacking

Clone the repository, install all Python dependencies, tox and some auxiliary tools with

```bash
pip3 install --user --force-reinstall -e .
sudo dnf install tox jq lolcat toilet
```

If you run into any problems that seem to suggest an out-of-date or missing
dependency, rerun the command above.

Please run

```bash
tox
```

before submitting any changes.

## Running a webhook for one merge request

Each webhook has documentation at the top of the file that describes the specifics of how to run
it on your system.

- [ACK/NACK](README.ack_nack.md)
- [Bugzilla](README.bugzilla.md)
- [External CI (CKI)](README.external_ci.md)
- [Pulbic Merge Request Handling](README.public.md)
- [Signoff](README.signoff.md)

You may need to set a few other options that's applicable to all webhooks:

- Debug logging output can be enabled by setting the environment variable
  `CKI_LOGGING_LEVEL=DEBUG`.
- If the repository you are writing to is not publicly-available, or you need
  to perform a write operation, then the GitLab personal token needs to be
  set in an environment variable.
      ```shell
      export COM_GITLAB_TOKEN='1234567890abcedf'
      export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
      ```
  The personal access token can be generated by logging into the GitLab web UI,
  click on your avatar at the top right, go to Settings, and Access Tokens on the left.
  At a minimum, you want to give your access token the `read_api`, and `read_repository`
  scopes.

  Without a proper access token set, you will get cryptic "404 not found" errors when running
  the webhook.
- Write operations can be enabled by setting the environment variable
  `IS_PRODUCTION='true'`. This requires setting the GitLab access token as described
  above.
- The webhook can be manually started to consume messages from RabbitMQ:
  ```bash
      python3 -m webhook.WEBHOOK_NAME \
              --rabbitmq-host public-rabbitmq-ark.apps.ocp.ci.centos.org \
              --rabbitmq-port 443 \
              --rabbitmq-user kernel \
              --rabbitmq-password PASSWORD \
              --rabbitmq-exchange cki.webhooks \
              --rabbitmq-routing-key ROUTING_KEY \
              --rabbitmq-queue-name QUEUE_NAME
  ```

## Design

Webhooks were designed to have a quick response time.  Due to the complexities
of what the webhooks need to do and the unstable nature of network
infrastructure, a message queue was introduced to aid in robustness.

A RabbitMQ server quickly receives the hook and broadcasts it to all the
listeners.  Any infrastructure issues fails to send the message ack,
therefore the message is resent.

In addition, some webhook data needs access to the internal Red Hat network.
The RabbitMQ allows encrypted communication for consumers inside the
firewall to access the message and respond appropriately to the public
gitlab server.

```
 +-------+      +----------+      +----------+
 |Gitlab |      | Webhook  |      | RabbitMQ |
 |Project|----->| receiver |----->|          |
 +-------+      +----------+      +----------+
                                       |
                   Public internet     |
                   --------------------|------------
                   Internal network    |
                                       |
                               +-------+-------+----------+
                               |               |          -
                               v               v          v
                           +--------+     +---------+
                           | Public |     |Bugzilla |    ...
                           | Queue  |     |Queue    |
                           +--------+     +---------+
```

Everything is managed in containers using OpenShift. The webhook receiver and
the RabbitMQ server run in CentOS CI. The containers handling the actual
webhook processing run inside the RH intranet. The service is managed by the
CKI team.


## How to create a new webhook

Separate documentation about how to create a new webhook is described in
[CREATE_NEW_WEBHOOK.md](CREATE_NEW_WEBHOOK.md).


## Inspecting Python Packaging

To inspect the python packaging, install the wheel package with
`dnf install python3-wheel` and then run `python3 setup.py bdist_wheel` to
generate a whl file (wheel) in the `dist/` directory. The wheel package is
just a ZIP file that can be inspected with `unzip -v`.

You can test installing that wheel package inside a container with the
following commands:

```bash
podman run -v ./dist:/package:z -ti fedora:33
# The following commands are executed inside the container
dnf install -y git python3-pip
pip3 install /package/kernel_webhooks-1-py3-none-any.whl 
pip3 show kernel-webhooks
```

## Deployment

This repository is setup to rebuild the containers after each merge and push them
to the registry.

These containers listen on a RabbitMQ message bus and do not filter on
wildcards for security reasons.  Instead they filter on requested events
based on `<location>.<group>.<project>.<event>`.  For example,

```
gitlab.com.cki-project.kernel-ark.merge_request
```

When adding new webhooks or events, please consult
<https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/secrets.yml>.

and verify `KERNEL_WEBHOOKS_<container>_ROUTING_KEYS` matches those events.
If not, add an entry per event and submit an MR for that tree.


# Viewing logs in OpenShift / sentry

- Ensure that your user has access to the
  [cki-ocp-view rover group](https://rover.redhat.com/groups/group/cki-ocp-view).

- Log into the OpenShift console at
  <https://console-openshift-console.apps.ocp4.prod.psi.redhat.com/>. Click the `my_ldap_provider`
  and login with your Kerberos credentials. On the left, click `Workloads`, `Pods`, and find the
  `kernel-webhooks` pods of interest. Note the status field since some will be running or completed.

- Alternatively, you can use the OpenShift CLI to view the pod status and logs. You can install
  the `origin-clients` RPM on Fedora, or a newer version of the CLI from
  <https://access.redhat.com/downloads/content/290>.

      oc login api.ocp4.prod.psi.redhat.com:6443
      oc get pods --namespace cki
      oc logs --namespace cki pod/kernel-webhooks-bugzilla-30-z8vdd

- Runtime Errors are logged and tracked in
  [sentry](https://sentry.engineering.redhat.com/baseos/kernel-webhooks/).


## Need help?

If you encountered a bug with the kernel-webhooks, or want a new feature, then open an issue
on the [kernel-webhooks issue tracker](https://gitlab.com/cki-project/kernel-webhooks/-/issues).
Direct any questions to the [mailto:rh-kwf@redhat.com](rh-kwf@redhat.com) mailinglist.
