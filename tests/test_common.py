"""Webhook interaction tests."""
import unittest
from unittest import mock

import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(unittest.TestCase):
    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = webhook.common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = []
        commits.append("abcdef012345")
        table = []
        table.append(["012345abcdef", commits, 1, "", ""])
        for row in table:
            commits = webhook.common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_extract_files(self):
        """Check function returns expected file list."""

        diff_a = {'new_path': 'net/dev/core.c'}
        diff_b = {'new_path': 'redhat/Makefile'}

        commit = mock.Mock()
        commit.diff.return_value = [diff_a, diff_b]
        self.assertEqual(webhook.common.extract_files(commit),
                         ['net/dev/core.c', 'redhat/Makefile'])
