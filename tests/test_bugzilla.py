"""Webhook interaction tests."""
import copy
import json
import os
import unittest
from unittest import mock

import webhook.bugzilla
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBugzilla(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment'},
                    'merge_request': {'target_branch': 'main', 'iid': 2}
                    }

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'BUGZILLA_ROUTING_KEYS': 'mocked', 'BUGZILLA_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        consume_value = [(mock.Mock(), 'foo', json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.bugzilla.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload=payload)

    def test_validate_internal_bz(self):
        """Check for expected return value."""
        commits = {"deadb": ['redhat/Makefile']}
        self.assertTrue(webhook.bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h']
        self.assertFalse(webhook.bugzilla.validate_internal_bz(commits))

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_MERGE)
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        self._test_payload(mocked_gitlab, True, payload=payload)

    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab):
        """Check handling of a note."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation"
        self._test_payload(mocked_gitlab, True, payload)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation_logs(self, mocked_gitlab):
        """Check handling of bz re-evaluation with logs."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation(logs)"
        self._test_payload(mocked_gitlab, True, payload)

    @mock.patch('gitlab.Gitlab')
    def test_pipeline(self, mocked_gitlab):
        """Check handling of a note."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload.update({'object_kind': 'pipeline'})
        payload["object_attributes"]["status"] = "success"
        self._test_payload(mocked_gitlab, True, payload)

    def _test_payload(self, mocked_gitlab, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, [])
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, [])

    # pylint: disable=too-many-arguments,too-many-locals
    def _test(self, mocked_gitlab, result, payload,
              labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        c1 = mock.Mock(id="1234",
                       message="1\n"
                       "Bugzilla: http://bugzilla.redhat.com?id=1234",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567",
                       message="2\nBugzilla: http://bugzilla.test?id=45678\n"
                       "Bugzilla:#4567\n"
                       "Depends: Bug#1234",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="890a",
                       message="3\nBugzilla: INTERNAL",
                       parent_ids=["face"])
        c4 = mock.Mock(id="face",
                       message="This is a merge",
                       parent_ids=["abcd", "4567"])
        c5 = mock.Mock(id="deadbeef",
                       message="5\nBugzilla: 1234567, 7654321\n"
                       "Nothing to see here\n"
                       "Bugzilla: http://bugzilla.redhat.com/1989898",
                       parent_ids=["890a"])
        c6 = mock.Mock(id="1800555",
                       message="6\nBugzilla: bz#1800555",
                       parent_ids=["deadbeef"])
        c7 = mock.Mock(id="e404",
                       message="7\n"
                       "https://bugzilla.redhat.com/show_bug.cgi?id=9983023",
                       parent_ids=["1800555"])
        c8 = mock.Mock(id="abcd",
                       message="8\n"
                       "Depends: "
                       "https://bugzilla.redhat.com/show_bug.cgi?id=7654321\n",
                       parent_ids=["1800555"])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6, c7, c8]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "face": c4,
                           "deadbeef": c5, "1800555": c6, "e404": c7,
                           "abcd": c8}
        self.assertEqual(webhook.bugzilla.extract_bzs(c1.message), ["1234"])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c1.message), [])
        self.assertEqual(webhook.bugzilla.extract_bzs(c2.message), ["4567"])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c2.message),
                         ["1234"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c3.message), ["INTERNAL"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c5.message),
                         ["1234567", "7654321", "1989898"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c6.message), ["1800555"])
        self.assertEqual(webhook.bugzilla.extract_bzs(c7.message), [])
        self.assertEqual(webhook.bugzilla.extract_dependencies(c8.message),
                         ["7654321"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No approved issue IDs referenced in log message or"
                     " changelog for HEAD\n*** Unapproved issue IDs referenced"
                     " in log message or changelog for HEAD\n*** Commit HEAD"
                     " denied\n*** Current checkin policy requires:\n"
                     "    release == +\n"
                     "*** See https://rules.doc/doc for more information",
            "result": "fail",
            "logs": "*** Checking commit HEAD\n*** Resolves:\n***   "
                    "Unapproved:\n***     rhbz#1234 (release?, qa_ack?, "
                    "devel_ack?, mirror+)\n"
        }

        with mock.patch('webhook.bugzilla.SESSION.post', return_value=presult):
            return_value = \
                webhook.common.process_message('dummy', payload,
                                               webhook.bugzilla.WEBHOOKS)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels),
                                 sorted(assert_labels))
        else:
            self.assertFalse(return_value)
