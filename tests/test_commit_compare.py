"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.commit_compare
import webhook.common

# a hunk of upstream linux kernel commit ID 1fc70edb7d7b5ce1ae32b0cf90183f4879ad421a
PATCH_A = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_A += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_A += "--- a/include/linux/netdevice.h\n"
PATCH_A += "+++ b/include/linux/netdevice.h\n"
PATCH_A += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_A += "        unsigned short          type;\n"
PATCH_A += "        unsigned short          hard_header_len;\n"
PATCH_A += "        unsigned char           min_header_len;\n"
PATCH_A += "+       unsigned char           name_assign_type;\n"
PATCH_A += "\n"
PATCH_A += "        unsigned short          needed_headroom;\n"
PATCH_A += "        unsigned short          needed_tailroom;\n"

PATCH_B = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_B += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_B += "--- a/include/linux/netdevice.h\n"
PATCH_B += "+++ b/include/linux/netdevice.h\n"
PATCH_B += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_B += "        unsigned short          type;\n"
PATCH_B += "        unsigned short          hard_header_len;\n"
PATCH_B += "        unsigned char           min_header_len;\n"
PATCH_B += "+       unsigned char           name_assign_type;\n"
PATCH_B += "\n"
PATCH_B += "        unsigned short          needed_max_headroom;\n"
PATCH_B += "        unsigned short          needed_tailroom;\n"

PATCH_C = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_C += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_C += "--- a/include/linux/netdevice.h\n"
PATCH_C += "+++ b/include/linux/netdevice.h\n"
PATCH_C += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_C += "        unsigned short          type;\n"
PATCH_C += "        unsigned short          hard_header_len;\n"
PATCH_C += "        unsigned char           min_header_len;\n"
PATCH_C += "+       RH_KABI_EXTEND(unsigned char name_assignee_type)\n"
PATCH_C += "\n"
PATCH_C += "        unsigned short          needed_headroom;\n"
PATCH_C += "        unsigned short          needed_tailroom;\n"


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommitCompare(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment'},
                    'merge_request': {'target_branch': 'main', 'iid': 2}
                    }

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab, udiff, sdiff):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, False, payload=payload)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab, udiff, sdiff):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, payload=payload)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab, udiff, sdiff):
        """Check handling of a note."""
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_ucid_re_evaluation(self, mocked_gitlab, udiff, sdiff):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT"
        self._test_payload(mocked_gitlab, True, payload)

    def test_get_match_info(self):
        """Check that we get sane strings back for match types."""
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.FULL)
        self.assertEqual(output, ("100% match", True))
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.KABI)
        self.assertEqual(output, ("kABI Diffs", False))

    def test_find_kabi_hints(self):
        """Check that we can find a kabi hint in a patch hunk."""
        output = webhook.commit_compare.find_kabi_hints(PATCH_A)
        self.assertFalse(output)
        output = webhook.commit_compare.find_kabi_hints(PATCH_C)
        self.assertTrue(output)

    def test_ucid_compare(self):
        """Check that diff engine actually compares things properly."""
        # compare two identical patches (should be equal)
        output = webhook.commit_compare.compare_commit_to_upstream(PATCH_A, PATCH_A)
        self.assertEqual(output, [])
        # compare two patches with only context differences (should be equal)
        output = webhook.commit_compare.compare_commit_to_upstream(PATCH_A, PATCH_B)
        self.assertEqual(output, [])
        # compare two patches with actual differences (should NOT be equal)
        output = webhook.commit_compare.compare_commit_to_upstream(PATCH_A, PATCH_C)
        self.assertNotEqual(output, [])

    def _test_payload(self, mocked_gitlab, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, [])
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, [])

    # pylint: disable=too-many-arguments
    def _test(self, mocked_gitlab, result, payload,
              labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        c1 = mock.Mock(id="1234", message="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c2 = mock.Mock(id="4567", message="2\ncommit 12345678")
        c3 = mock.Mock(id="890a", message="3\nUpstream: RHEL-only")
        c4 = mock.Mock(id="deadbeef", message="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)")
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4}
        self.assertEqual(webhook.commit_compare.extract_ucid(c1.message),
                         ["1234567890abcdef1234567890abcdef12345678"])
        self.assertEqual(webhook.commit_compare.extract_ucid(c2.message), [])
        self.assertEqual(webhook.commit_compare.extract_ucid(c3.message), ["RHEL-only"])
        self.assertEqual(webhook.commit_compare.extract_ucid(c4.message),
                         ["abcdef0123456789abcdef0123456789abcdef01"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No upstream commit ID found in submitted patch!\n***",
            "result": "fail",
            "logs": "*** Comparing submitted patch with upstream commit ID\n***"
        }

        with mock.patch('webhook.commit_compare.SESSION.post', return_value=presult):
            return_value = \
                webhook.common.process_message('dummy', payload,
                                               webhook.commit_compare.get_webhooks("/src/linux"))

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))
        else:
            self.assertFalse(return_value)
