"""Query MRs for upstream commit IDs to compare against submitted patches."""
import difflib
import enum
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
# GitPython
from git import Git
from git import Repo

from . import common

LOGGER = logger.get_logger('cki.webhook.commit_compare')
SESSION = session.get_session('cki.webhook.commit_compare', LOGGER)


def extract_ucid(message):
    """Extract upstream commit ID from the message."""
    message_lines = message.split('\n')
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gitlog_re = re.compile('^commit [a-f0-9]{40}$')
    # Pattern for 'git cherry-pick -x <commit>' based backports
    gitcherrypick_re = re.compile(r'^\(cherry picked from commit [a-f0-9]{40}\)$')
    # Bare hash matching pattern
    githash_re = re.compile('[a-f0-9]{40}', re.IGNORECASE)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile('^Upstream:.*RHEL.*only', re.IGNORECASE)

    for line in message_lines:
        gitrefs = gitlog_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        gitrefs = gitcherrypick_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        if rhelonly_re.findall(line):
            gitref_list.append("RHEL-only")
    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    return gitref_list


def noucid_msg():
    """Return the message to use as a note for commits without an upstream commit ID."""
    msg = "These commits have no valid upstream commit ID, please add a line to the "
    msg += "the changelog in the format \'commit <40-char commit ID>\'."
    return msg


def rhelonly_msg():
    """Return the message to use as note for commits that claim to be RHEL-only."""
    msg = "These commits claim to be RHEL-only, without any corresponding upstream "
    msg += "commit, and should be evaluated accordingly."
    return msg


def unk_cid_msg():
    """Return the message to use as note for commits that have an unknown commit ID."""
    msg = "These commits reference an upstream commit ID, but its source of origin "
    msg += "is not recognized. Please confirm the upstream tree it is from."
    return msg


def diffs_msg():
    """Return the message to use as note for commits that differ from upstream."""
    msg = "These commits differ from their referenced upstream commits, and should "
    msg += "be evaluated accordingly."
    return msg


def kabi_msg():
    """Return the message to use as note for commits that may contain kABI work-arounds."""
    msg = "These commits appear to contain kABI work-arounds, and should be carefully "
    msg += "evaluated accordingly."
    return msg


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4


def get_report_table(reviewed_items):
    """Create the table for the upstream commit ID report."""
    mr_approved = True
    table = []
    for cid in reviewed_items:
        ucid_properties = reviewed_items[cid]["validation"]
        mr_approved = mr_approved and ucid_properties['match'] != Match.NOUCID
        commits = []
        for commit in reviewed_items[cid]["commits"]:
            commits.append(commit)
        table.append([cid, commits, ucid_properties['match'],
                      ucid_properties['notes'], ucid_properties['logs']])
    return (table, mr_approved)


def get_match_info(match):
    """Get info on match type."""
    mstr = "No UCID   "
    full_match = True
    if match == Match.FULL:
        mstr = "100% match"
    elif match == Match.PARTIAL:
        mstr = "Partial   "
        full_match = False
    elif match == Match.DIFFS:
        mstr = "Diffs     "
        full_match = False
    elif match == Match.KABI:
        mstr = "kABI Diffs"
        full_match = False
    return (mstr, full_match)


def print_gitlab_report(notes, table, mr_approved):
    """Print an upstream commit ID mapping report for gitlab."""
    kerneloscope = "http://kerneloscope.usersys.redhat.com"
    full_match = True
    report = "<br>\n\n**Upstream Commit ID Readiness Report**\n\n"
    report += "Note that not matching does not necessarily mean the backport "
    report += "had any problems, just that it did not match the referenced "
    report += "upstream commit ID 100%. Matching is not a guaranteee of "
    report += "correctness either, due to possible kABI concerns."
    report += " \n"
    report += "* Patches that match upstream 100% not shown\n\n"
    report += "|UCID    |Sub CID |Match     |Notes   |\n"
    report += "|:-------|:-------|:---------|:-------|\n"
    for row in table:
        if row[2] == Match.FULL:
            continue
        if row[0] != "-" and row[2] != Match.NOUCID:
            ucidh = f"[{row[0][:8]}]({kerneloscope}/commit/{row[0]})"
        else:
            ucidh = f"{row[0][:8]}"
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        report += "|"+ucidh+"|"+"<br>".join(commits)
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request upstream commit IDs all present.\n"
        if full_match:
            report += "All patches match referenced upstream commit IDs 100%.\n"
        else:
            report += "Please note and evaluate differences from upstream.\n"
    else:
        report += "\nMerge Request missing upstream commit IDs\n"
        report += " \n"
        report += "To request re-evalution after getting upstream commit(s) "
        report += "reachable, add a comment to this MR with only the text: "
        report += "request-commit-id-evaluation "

    return report


def print_text_report(notes, table, mr_approved):
    """Print an upstream commit ID mapping report for the text console."""
    full_match = True
    report = "\n\nUpstream Commit ID Readiness Report\n"
    report += "* Patches that match upstream 100% not shown\n\n"
    report += "|UCID    |Sub CID |Match     |Notes   |\n"
    report += "|:-------|:-------|:---------|:-------|\n"
    for row in table:
        if row[2] == Match.FULL:
            continue
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0][:8])+"|"+" ".join(commits)
        (mstr, match_status) = get_match_info(row[2])
        full_match = full_match and match_status
        report += "|"+mstr+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += "Merge Request %s upstream commit ID validation.\n" % status
    if full_match:
        report += "All patches match referenced upstream commit IDs 100%\n"
    if mr_approved and not full_match:
        report += "However, please verify differences from upstream.\n"
    return report


def build_review_lists(mri, ucid, review_lists, cid):
    """Build review_lists for this commit ID."""
    try:
        found_files = common.extract_files(mri.project.commits.get(cid))
    # pylint: disable=broad-except
    except Exception:
        found_files = []

    cid_data = review_lists[cid] if cid in review_lists else {}
    commits = cid_data["commits"] if "commits" in cid_data else {}
    commits[ucid] = found_files
    cid_data["commits"] = commits
    return cid_data


def search_compiled(line, re_list, group0=False, first=True):
    """Actual regex matching happens here."""
    matches = []
    for item in re_list:
        match = item.search(line)
        if match:
            if group0 and match.group(0):
                matches.append(match.group(0))
            elif not group0 and match.group(1):
                matches.append(match.group(1))
            if first:
                break

    return matches


def filter_diff(diff):
    """Filter the diff, isolating only the kinds of differences we care about."""
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_header = True
    hit = False
    header = []
    new_diff = []
    cache = []
    diff_compiled = []

    # sort through rules to see if this chunk is worth saving
    # most of the filters are patch header junk.  Don't care if those
    # offsets are different.
    # The last filter is the most interesting: filter context differences
    # Basically that just filters out noise that changed around
    # the patch and not the parts of the patch that does anything.
    # IOW, the lines in a patch with _no_ ± in front.
    # Personal preference if that is interesting or not.

    diff_re = ["^[+|-]index ",
               "^[+|-]--- ",
               r"^[+|-]\+\+\+",
               "^[+|-]diff ",
               "^[+|-]$",
               "^[+|-]@@ ",
               "^[+|-] "]

    for prefix in diff_re:
        diff_compiled.append(re.compile(prefix))

    # end pre-compile stuff

    for line in diff:

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # filter lines
        if line[0] == " ":
            # patch context, ignore
            continue
        if search_compiled(line, diff_compiled, group0=True):
            # hit junk, skip
            continue

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    return new_diff


def compare_commit_to_upstream(udiff, cdiff):
    """Perform interdiff on the upstream and submitted patches' diffs."""
    interdiff = difflib.unified_diff(udiff.split('\n'), cdiff.split('\n'),
                                     fromfile='upstream', tofile='submission', lineterm="")
    interesting = filter_diff(interdiff)
    return interesting


def get_upstream_diff(mri, ucid):
    """Extract diff for upstream commit ID."""
    repo = Repo(mri.linux_src)

    try:
        commit = repo.commit(ucid)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.debug("Commit ID %s not found in upstream", ucid)
        return None

    linux = Git(mri.linux_src)
    diff = linux.diff("%s^" % commit, commit, '--no-renames')
    return diff


def get_submitted_diff(mri, cid):
    """Extract diff for submitted patch."""
    diff = ""

    if len(cid) != 40:
        return f'Invalid commit id: {cid}'

    commit = mri.project.commits.get(cid)
    for path in commit.diff():
        diff += "diff --git a/" + path['old_path'] + " "
        diff += "b/" + path['new_path'] + "\n"
        diff += "index blahblah..blahblah 100644\n"
        diff += "--- a/" + path['old_path'] + "\n"
        diff += "+++ b/" + path['new_path'] + "\n"
        diff += path['diff']

    return diff


def find_kabi_hints(diff):
    """Check for hints this patch has kABI workarounds in it."""
    if "genksyms" in diff.lower() or "rh_kabi" in diff.lower():
        return True
    return False


def validate_commit_ids(mri, review_lists):
    """Iterate through the upstream commit IDs we found and compare w/our submitted commits."""
    notes = []
    noucid_id = ""
    rhelonly_id = ""
    unknown_id = ""
    diffs_id = ""
    kabi_id = ""
    idiff = ""

    for ucid in review_lists:
        ucid_properties = {'match': Match.NOUCID, 'notes': [], 'logs': ''}
        review_lists[ucid]["validation"] = ucid_properties

        if ucid == "-":
            notes += [] if noucid_id else [noucid_msg()]
            noucid_id = noucid_id if noucid_id else str(len(notes))
            ucid_properties['notes'].append(noucid_id)
            continue

        if ucid == "RHEL-only":
            notes += [] if rhelonly_id else [rhelonly_msg()]
            rhelonly_id = rhelonly_id if rhelonly_id else str(len(notes))
            ucid_properties['notes'].append(rhelonly_id)
            continue

        udiff = get_upstream_diff(mri, ucid)
        if udiff is None:
            notes += [] if unknown_id else [unk_cid_msg()]
            unknown_id = unknown_id if unknown_id else str(len(notes))
            ucid_properties['notes'].append(unknown_id)
            continue

        for cid in review_lists[ucid]["commits"]:
            mydiff = get_submitted_diff(mri, cid)
            idiff = compare_commit_to_upstream(udiff, mydiff)
            for line in idiff:
                LOGGER.debug("idiff: %s", line)
        if len(idiff) == 0:
            ucid_properties['match'] = Match.FULL
        else:
            if find_kabi_hints(mydiff):
                ucid_properties['match'] = Match.KABI
                notes += [] if kabi_id else [kabi_msg()]
                kabi_id = kabi_id if kabi_id else str(len(notes))
                ucid_properties['notes'].append(kabi_id)
            else:
                ucid_properties['match'] = Match.DIFFS
                notes += [] if diffs_id else [diffs_msg()]
                diffs_id = diffs_id if diffs_id else str(len(notes))
                ucid_properties['notes'].append(diffs_id)
    return notes


def check_on_ucids(mri):
    """Check upstream commit IDs."""
    review_lists = {}
    for commit in mri.merge_request.commits():
        try:
            found_refs = extract_ucid(commit.message)
        # pylint: disable=broad-except
        except Exception:
            found_refs = []

        for ucid in found_refs:
            review_lists[ucid] = build_review_lists(mri, commit.id,
                                                    review_lists, ucid)

        if not found_refs:
            review_lists["-"] = build_review_lists(mri, commit.id,
                                                   review_lists, "-")

    notes = validate_commit_ids(mri, review_lists)
    (table, approved) = get_report_table(review_lists)
    return (notes, table, approved)


# pylint: disable=too-many-instance-attributes
class MRUCIDInstance:
    """Merge request Instance."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the commit ID comparison instance."""
        self.linux_src = "/usr/src/linux"
        self.lab = lab
        self.project = project
        self.merge_request = merge_request
        self.payload = payload

    def update_mr(self):
        """Update the merge request if production."""
        if misc.is_production():
            self.merge_request.save()
        else:
            LOGGER.info('Skipping merge request update in non-production')

    def run_ucid_validation(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.info("Running upstream commit ID validation\n")
        LOGGER.debug("Merge request description:\n%s",
                     self.merge_request.description)
        (notes, table, approved) = check_on_ucids(self)
        if misc.is_production():
            report = print_gitlab_report(notes, table, approved)
            self.merge_request.notes.create({'body': report})
        else:
            LOGGER.info('Skipping adding upstream commit ID report in non-production')
            report = print_text_report(notes, table, approved)
            LOGGER.info(report)
        if not approved:
            LOGGER.info("Some commits don't match upstream 100%, please inspect manually!\n")


def get_mri(gl_instance, message, key):
    """Return a merge request instance for the webhook payload."""
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(message.payload[key]["iid"])
    return MRUCIDInstance(gl_instance, gl_project, gl_mergerequest, message.payload)


def process_mr(gl_instance, message, linux_src):
    """Process a merge request message."""
    mri = get_mri(gl_instance, message, "object_attributes")
    if mri:
        mri.linux_src = linux_src
        mri.run_ucid_validation()
        mri.update_mr()


def process_note(gl_instance, message, linux_src):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    mri = get_mri(gl_instance, message, "merge_request")
    notetext = message.payload['object_attributes']['note']

    if notetext != "request-commit-id-evaluation":
        return

    if mri:
        mri.linux_src = linux_src
        mri.run_ucid_validation()
        mri.update_mr()


def get_webhooks(linux_src):
    """Return the supported webhooks to listen on."""
    # The key should be the value GitLab uses in the object_kind parameter in
    # the payload of the web hook request.
    # https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
    return {
      'merge_request': lambda gl_instance, msg: process_mr(gl_instance, msg, linux_src),
      'note': lambda gl_instance, msg: process_note(gl_instance, msg, linux_src),
    }


def process_note_url(args):
    """Process the given note for a merge request url."""
    payload = common.make_payload(args, "note")
    payload["object_attributes"]["note"] = args.note
    payload["merge_request"] = {}
    payload["merge_request"]["iid"] = payload["_mr_id"]
    del payload["_mr_id"]
    common.process_message('cmdline', payload, get_webhooks(args.linux_src))


def process_merge_request_url(args):
    """Process a merge request url."""
    payload = common.make_payload(args.merge_request, "merge_request")
    payload["object_attributes"]["iid"] = payload["_mr_id"]
    del payload["_mr_id"]
    common.process_message('cmdline', payload, get_webhooks(args.linux_src))


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--note', help='Process a note for the given merge request')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    if args.merge_request and args.note:
        process_note_url(args)
        return
    if args.merge_request:
        process_merge_request_url(args)
        return

    common.consume_queue_messages(args, get_webhooks(args.linux_src))


if __name__ == "__main__":
    main(sys.argv[1:])
