"""Ensure MR commits have the necessary DCO signoff."""
import enum
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import common

LOGGER = logger.get_logger('cki.webhook.signoff')
SESSION = session.get_session('cki.webhook.signoff', LOGGER)

DCO_URL = "https://developercertificate.org"
DCO_PASS = "The DCO Signoff Check for all %s commits has **PASSED**.\n"
DCO_FAIL = ("**ERROR: DCO 'Signed-off-by:' tags were not found on all commits. Please review the "
            "results in the table below.**  \n"
            "This project requires developers add a per-commit acknowlegement of the [Developer "
            "Certificate of Origin](%s), also known as the DCO. This can be accomplished by "
            "adding an explicit 'Signed-off-by:' tag to each commit.\n\n"
            "**This Merge Request's commits will not be considered for inclusion into this "
            "project until these problems are resolved. After making the required changes please "
            "resubmit your merge request for review.**\n\n" % (DCO_URL))


class State(enum.IntEnum):
    """Possible commit DCO check results."""

    OK = 0
    MISSING = 1
    NOMATCH = 2


footnotes = {
    # State.OK: "A valid DCO Signoff was found for this commit.",

    State.MISSING: "No DCO Signoff was found for this commit.",
    State.NOMATCH: "The committer's email address does not match the commit Signoff email address.",
}


def get_current_signoff_scope(labels):
    """Get current Signoff label scope."""
    for label in labels:
        if label.startswith("Signoff::"):
            return label.split(":")[2]
    return None


def find_dco(commit):
    """Look for DCO string and return state."""
    state = State.MISSING
    expected_dco = f"Signed-off-by: {commit.author_name} <{commit.author_email}>"
    mlines = commit.message.splitlines()
    for line in mlines:
        if line.rstrip() == expected_dco:
            state = State.OK
            break
        if line.startswith("Signed-off-by:"):
            state = State.NOMATCH
    LOGGER.debug("Expected DCO: '%s' - %s", expected_dco, state.name)
    return state


def process_commits(project, mr_commits):
    """For a given set of commits return a dict of {commit.id: State}."""
    commits = {}
    for commit in mr_commits:
        cid = commit.id
        commit = project.commits.get(cid)
        # Skip merge commits
        if len(commit.parent_ids) > 1:
            LOGGER.debug("Merge commit? Skipping.")
            continue
        commits[cid] = find_dco(commit)
    return commits


def generate_table(commits):
    """Generate a markdown table of results."""
    # header
    results_table = "**DCO Signoff Report**\n\n"
    if all(state == State.OK for state in commits.values()):
        results_table += DCO_PASS % (len(commits))
        if misc.is_production():
            return results_table
    else:
        results_table += DCO_FAIL
    # table of results
    results_table += "| **Commit** | **Signoff Status** |\n|----|----|\n"
    for commit, state in commits.items():
        if state is State.OK:
            if not misc.is_production():
                results_table += "| %s | %s |\n" % (commit, state.name)
        else:
            results_table += "| %s | **%s** [^%d] |\n" % (commit, state.name, state.value)
    # footer (footnotes)
    for state, text in footnotes.items():
        results_table += "[^%d]: %s\n" % (state.value, text)
    return results_table


def update_mr(merge_request, results_table):
    """Update the MR with a note of the results and possibly set the label scope."""
    current_scope = get_current_signoff_scope(merge_request.labels)
    if "PASSED" in results_table:
        new_scope = "OK"
    else:
        new_scope = "NeedsReview"
    LOGGER.debug("current scope: '%s', new scope: '%s'", current_scope, new_scope)
    # This removal should be unnecessary when our GL instance has proper
    # label scope support.
    if new_scope != current_scope and current_scope:
        merge_request.labels.remove("Signoff::%s" % (current_scope))
    merge_request.labels.append("Signoff::%s" % (new_scope))
    if not misc.is_production():
        return
    merge_request.save()
    merge_request.notes.create({'body': results_table})


def process_mr(gl_instance, message):
    """Process a merge request message."""
    # Only run on 'open' action or 'update' action when 'oldrev' is set.
    action = message.payload['object_attributes']['action']
    if action == 'update' and 'oldrev' not in message.payload['object_attributes']:
        LOGGER.debug("Ignoring \'update\' action without an oldrev.")
        return
    if action not in ('update', 'open'):
        LOGGER.debug("Ignoring action '%s'.", action)
        return

    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(message.payload["object_attributes"]["iid"])

    processed_commits = process_commits(gl_project, gl_mergerequest.commits())
    results_table = generate_table(processed_commits)
    update_mr(gl_mergerequest, results_table)

    LOGGER.debug("Results table:\n%s", results_table)
    LOGGER.debug("Final labels: %s", gl_mergerequest.labels)


WEBHOOKS = {
    "merge_request": process_mr,
}


def process_merge_request_url(mr_url):
    """Process a merge request url."""
    payload = common.make_payload(mr_url, "merge_request")
    payload['object_attributes']['action'] = 'open'
    common.process_message('cmdline', payload, WEBHOOKS)


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SIGNOFF')
    args = parser.parse_args(args)
    if args.merge_request:
        process_merge_request_url(args.merge_request)
        return

    common.consume_queue_messages(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
